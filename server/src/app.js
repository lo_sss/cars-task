const express = require('express');
const mongoose = require('mongoose');
const carsRouter = require('./routes/cars');
const regionsRouter = require('./routes/regions');
const cors = require('cors');
require('dotenv').config();

// const url = 'mongodb://localhost/CarsDBex';

const app = express();

const corsOptions = {
    origin: process.env.CORS_ORIGIN,
    optionsSuccessStatus: 200,
}
app.use(cors(corsOptions));

mongoose.connect(process.env.URI, { useNewUrlParser: true });
const con = mongoose.connection;

con.on('open', () => {
    console.log('Connected...')
})

app.use(express.json());

app.use('/cars', carsRouter);
app.use('/regions', regionsRouter);

app.listen(process.env.DB_PORT, () => {
    console.log('Server started...')
})