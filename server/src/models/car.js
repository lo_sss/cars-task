const mongoose = require('mongoose');
const moment = require('moment-timezone');

const dateBul = moment.tz(Date.now(), "Europe/Athens").format('YYYY-MM-DD');

const carSchema = new mongoose.Schema({
    brand: {
        type: String,
        required: true,
        minlength: 1, 
        maxlength: 20
    },
    region: {
        type: String,
        required: true
    },
    createdOn: {
        type: String, 
        default: dateBul
    }
});

module.exports = module.exports = mongoose.model('Car', carSchema);