const mongoose = require('mongoose');
const moment = require('moment-timezone');

const dateBul = moment.tz(Date.now(), "Europe/Athens").format('YYYY-MM-DD');

const regionSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    createdOn: {
        type: String, 
        default: dateBul
    }
});

module.exports = mongoose.model('Region', regionSchema);