const express = require("express");
const Car = require("../models/car");

const carsRouter = express.Router();

carsRouter.get("/", async (req, res) => {
  try {
    const cars = await Car.find();
    res.json(cars);
  } catch (err) {
    res.send(err);
  }
});

carsRouter.post("/", async (req, res) => {
  const body = new Car({
    brand: req.body.brand,
    region: req.body.region,
  });

  try {
    const car = await body.save();
    res.json(car);
  } catch (err) {
    res.send(err);
  }
});

carsRouter.delete("/:id", async (req, res) => {
  try {
    await Car.deleteOne({ _id: req.params.id });
    res.send(`Car with id ${req.params.id} was deleted!`);
  } catch (err) {
    res.send(err);
  }
});

module.exports = carsRouter;
