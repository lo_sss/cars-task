const Router = require('express');
const Region = require('../models/region')

const regionsRouter = Router();

regionsRouter.get("/", async (req, res) => {
    try {
      const regions = await Region.find();
      res.json(regions);
    } catch (err) {
      res.send(err);
    }
  });

module.exports = regionsRouter;