const mongoose = require("mongoose");
const Region = require('./src/models/region');
require('dotenv').config();

mongoose
    .connect(process.env.URI, {
        useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
        console.log('Mongo connection open!');
    })
    .catch((err) => {
        console.log('Error: ' + err)
    });

    const seedRegions = [
        {
            name: 'Africa'
        },
        {
            name: 'Asia'
        },
        {
            name: 'Europe'
        },
        {
            name: 'North America'
        },
        {
            name: 'South America'
        },
        {
            name: 'Antarctica'
        },
        {
            name: 'Australia'
        }
    ];

    const seedDB = async () => {
        await Region.deleteMany({});
        await Region.insertMany(seedRegions);
    };

    seedDB().then(() => {
        console.log('Mongo populated!');
        mongoose.connection.close();
    });