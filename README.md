# CARS-TASK

#### Technologies: MongoDB, Express, React, Node.js

## Need to have installed

* Node.js + npm
* MongoDB
* Docker + Docker Compose

## Functionalities

* add car
* list cars in table
* filter cars by brand/region
* delete car
# Installation
1. Clone the repo
2. In server directory create file .env 
 

 .env content


    URI=mongodb://mongo:27017/CarsDBex
    CORS_ORIGIN=http://localhost:3000
    DB_PORT=9000

3. $ docker compose up


