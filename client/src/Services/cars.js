import axios from "axios";

import { url } from "../constants";

export const postCar = (newCar) => axios.post(`${url}cars`, newCar);

export const getCars = () => axios(`${url}cars`);

export const deleteCar = (id) => axios.delete(`${url}cars/${id}`);
