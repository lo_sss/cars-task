import axios from "axios";

import { url } from "../constants";

export const getRegions = () => axios(`${url}regions`);