import "./PageNotFound.css";

export const PageNotFound = () => {
  return (
    <div className="page-not-found">
      <img
        src="https://firebasestorage.googleapis.com/v0/b/our-gaming-forum.appspot.com/o/projectImages%2F404-error-template-3.png?alt=media&token=a2e2047f-60f9-44cf-a8fc-3b87afe707c1"
        alt=""
      />
    </div>
  );
}