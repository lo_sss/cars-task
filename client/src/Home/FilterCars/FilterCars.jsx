import { ButtonGroup, Dropdown, DropdownButton } from "react-bootstrap";
import { Funnel } from "react-bootstrap-icons";

import "./FilterCars.css";

export const FilterCars = ({ filterKeys, cars, onChange }) => {
  const handleSelect = (e) => {
    if (e === "*") return onChange(cars);

    return onChange(filterCars(e));
  };

  const filterCars = (value) => {
    return cars.filter((car) => car.brand === value || car.region === value);
  };

  return (
    <span className="funnel">
      <DropdownButton
        as={ButtonGroup}
        key="Secondary"
        id={`dropdown-variants-Secondary`}
        variant="secondary"
        size="sm"
        title={<Funnel />}
        onSelect={handleSelect}
      >
        <span className="drop-down-options">
          <Dropdown.Item eventKey="*">All</Dropdown.Item>
          <Dropdown.Divider />
          {filterKeys.map((item, key) => (
            <Dropdown.Item key={key} eventKey={item}>
              {item}
            </Dropdown.Item>
          ))}
        </span>
      </DropdownButton>
    </span>
  );
};
