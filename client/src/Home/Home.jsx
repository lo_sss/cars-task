import { Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

import { NewCar } from "../NewCar/NewCar";
import { CarsList } from "./CarsList/CarsList";
import "./Home.css";

export const Home = () => {
  const navigate = useNavigate();

  const handleClick = () => {
    navigate("/new-car");
    return <NewCar />;
  };

  return (
    <div className="area">
      <h2 className="header-style">Cars</h2>
      <Button
        className="align-button"
        variant="secondary"
        onClick={() => handleClick()}
      >
        Add Car
      </Button>
      <CarsList />
    </div>
  );
};
