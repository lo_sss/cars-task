import { useEffect } from "react";
import { useState } from "react";
import { Trash } from "react-bootstrap-icons";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";

import { deleteCar, getCars } from "../../Services/cars";
import { getRegions } from "../../Services/regions";
import { FilterCars } from "../FilterCars/FilterCars";
import "./CarsList.css";

export const CarsList = () => {
  const [cars, setCars] = useState([]);
  const [curCars, setCurCars] = useState([]);
  const [regions, setRegions] = useState([]);

  useEffect(() => {
    getRegions()
      .then((res) => setRegions(res.data.map((region) => region.name)))
      .catch((err) => console.error(err));
  }, []);

  useEffect(() => {
    getCars()
      .then((res) => {
        setCars(res.data);
        setCurCars(res.data);
      })
      .catch((err) => console.error(err));
  }, []);

  const deleteRow = (row) => {
    deleteCar(row._id);
    const newCars = cars.filter((car) => car._id !== row._id);
    const newCurCars = curCars.filter((car) => car._id !== row._id);
    setCars(newCars);
    setCurCars(newCurCars);
  };

  const showDeleteIcon = (data, row) => (
    <span className="funnel">
      <Trash onClick={() => deleteRow(row)} />
    </span>
  );

  const attachTitle = (title) => {
    const brands = cars
      .map((car) => car.brand)
      .filter((item, index, array) => array.indexOf(item) === index);

    const filterKeys = title === "Region" ? regions : brands;

    return (
      <div>
        <span className="title-style">{title}</span>
        <FilterCars
          filterKeys={filterKeys}
          cars={cars}
          onChange={(filteredCars) => setCurCars(filteredCars)}
        />
      </div>
    );
  };

  const columns = [
    {
      dataField: "_id",
      text: "CarID",
    },
    {
      dataField: "brand",
      text: "Brand",
      headerFormatter: () => attachTitle("Brand"),
    },
    {
      dataField: "region",
      text: "Region",
      headerFormatter: () => attachTitle("Region"),
    },
    {
      dataField: "createdOn",
      text: "Created on",
    },
    {
      dataField: "actions",
      text: "Actions",
      formatter: showDeleteIcon,
    },
  ];

  return (
    <div className="table-style">
      <BootstrapTable
        keyField="_id"
        data={curCars}
        columns={columns}
        striped
        hover
        condensed
        pagination={paginationFactory()}
      />
    </div>
  );
};
