import { useEffect, useState } from "react";
import { Alert, Card, Form } from "react-bootstrap";
import { Button } from "react-bootstrap";
import { XSquare } from "react-bootstrap-icons";
import { useNavigate } from "react-router-dom";

import { postCar } from "../Services/cars";
import { getRegions } from "../Services/regions";
import "./NewCar.css";

export const NewCar = () => {
  const [newCar, setNewCar] = useState({
    brand: "",
    region: "",
  });
  const [regions, setRegions] = useState([]);
  const [success, setSuccess] = useState("");
  const [error, setError] = useState("");
  const navigate = useNavigate();

  useEffect(() => {
    getRegions()
      .then((res) => setRegions(res.data.map((region) => region.name)))
      .catch((err) => console.error(err));
  }, []);

  const updateForm = (prop) => (e) => {
    setNewCar({
      ...newCar,
      [prop]: e.target.value,
    });

    setError("");
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (newCar.brand.length < 1 || newCar.brand.length > 20) {
      return setError("Car brand must be up to 20 symbols");
    }

    if (newCar.region.length < 1) {
      return setError("You must choose region from the options");
    }

    e.currentTarget.disabled = true;

    postCar(newCar)
      .then((response) => {
        setNewCar(response.data);
        setSuccess("Successfully added!");
      })
      .catch((err) => setError(`${err.message}`));

    setTimeout(() => {
      navigate("/");
    }, 3000);
  };

  return (
    <Card className="card-style">
      <span className="close" onClick={() => navigate("/")}>
        <XSquare />
      </span>
      <Card.Body className="body-style">
        <h4 className="header">Add new car</h4>
        {error && <Alert variant="danger">{error}</Alert>}
        {success && <Alert variant="success">{success}</Alert>}
        <Form>
          <Form.Group className="mb-3">
            <Form.Label>Brand *</Form.Label>
            <Form.Control
              type="text"
              value={newCar.brand}
              onChange={updateForm("brand")}
              required
            />
          </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label>Region *</Form.Label>
            <Form.Select onChange={updateForm("region")}>
              <option>Choose region</option>
              {regions.map((region, key) => (
                <option key={key}>{region}</option>
              ))}
            </Form.Select>
          </Form.Group>
          <Button
            className="submit-style"
            variant="secondary"
            onClick={handleSubmit}
          >
            Add Car
          </Button>
        </Form>
      </Card.Body>
    </Card>
  );
};
