import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import { Home } from "./Home/Home";
import { NewCar } from "./NewCar/NewCar";
import { PageNotFound } from "./PageNotFound/PageNotFound";

function App() {
  return (
    <Router>
      <Routes>
        <Route index element={<Home />} />
        <Route path="new-car" element={<NewCar />} />
        <Route path="*" element={<PageNotFound />} />
      </Routes>
    </Router>
  );
}

export default App;
